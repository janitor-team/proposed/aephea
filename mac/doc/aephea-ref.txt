aephea-ref(7)                   MISCELLANEOUS                    aephea-ref(7)



  NAME
      aephea-ref - A description of the Aephea definitions for referencing

  DESCRIPTION
      The following definitions are used by the Aephea simpledocument class as
      well as the Portable Unix Documentation (pud)  manual  and  faq  classes
      (see pud-man(7) and pud-faq(7)).

  MACROS
      \refload#2
      \refloadx#2
        Both  macros  take as first argument a label that uniquely defines the
        reference.  The second  argument  is  a  vararg  containing  key-value
        pairs.

        These  macros are usually not needed in Aephea classes; they are lower
        level facilities.

      \ref#2
        The first argument is a label. The second argument  should  be  a  key
        associated  with  that  label. The result is the value associated with
        the label/key pair.

        These macros are usually not needed in Aephea classes; they are  lower
        level  facilities used by high level Aephea definitions (e.g. \iref#2,
        and \secref#1).

      \refcaption#1
      \refnumber#1
      \reflevel#1
        The argument is a label. The result is respectively the caption,  num-
        ber, or level associated with that label.

      \reference#1
      \refer#1
        The  first  associates  a  number  with the label that is in the first
        argument, and  prints  it  inbeween  square  brackets.  The  label  is
        anchored  to  this  location. The second outputs the associated number
        inbetween brackets, and makes it carry a link to the anchor. For exam-
        ple, I am about to create a link [1] and another link [2] to the items
        appearing below.

        [1] The first reference.

        [2] The second reference.

        If you prefer another bibliography style, find the  macros  and  adapt
        them according to your needs - they are very simple.

        \reference#2  currently writes data to the file \__fnbase__.zmr, to be
        read back in using a \load{\__fnbase__.zmr} statement.  This is  auto-
        matically done when using any of the Aephea classes.



  aephea-ref 1.002, 10-008          8 Jan 2010                     aephea-ref(7)
